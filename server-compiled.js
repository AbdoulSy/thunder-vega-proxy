'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressHttpProxy = require('express-http-proxy');

var _expressHttpProxy2 = _interopRequireDefault(_expressHttpProxy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express2.default)();
const PROXY_PORT = 3465;

app.use('/thunder', (0, _expressHttpProxy2.default)('http://localhost:3000'));
app.use('/vega', (0, _expressHttpProxy2.default)('http://localhost:8080'));

app.listen(PROXY_PORT, () => {
  console.log(`app listening on port ${PROXY_PORT}!`);
});
