import express from 'express'
import proxy from 'express-http-proxy'

const app = express()
const PROXY_PORT = 3465

app.use('/thunder', proxy('http://localhost:3000'))
app.use('/vega', proxy('http://localhost:8080'))

app.listen(PROXY_PORT, () => {
  console.log(`app listening on port ${PROXY_PORT}!`)
})
